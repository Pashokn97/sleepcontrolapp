from flask import Flask, render_template, request, redirect
from services import get_data_from_file, add_data_to_file, delete_data_from_file


sleep_app = Flask(__name__)


@sleep_app.route("/")
def get_data():
    data_from_file = get_data_from_file()
    return render_template("get_table.html", data_from_file=data_from_file)


@sleep_app.route("/add", methods=["POST", "GET"])
def add_data():
    if request.method == "POST":
        add_data_to_file(
            event=request.form["event"],
            date=request.form["date"],
            time=request.form["time"],
        )
        return redirect("/")
    return render_template("form_for_data.html")


@sleep_app.route("/delete", methods=["POST"])
def delete_data():
    delete_data_from_file(
        event=request.form["event"],
        date=request.form["date"],
        time=request.form["time"],
    )
    return redirect("/")


if __name__ == "__main__":
    sleep_app.run(debug=True)
