import json
from datetime import datetime
from json import JSONDecodeError

import logging

from settings import data_file


def preparing_to_sort(item: dict) -> tuple:
    return item["date"], item["time"]


def format_to_datetype(list_to_format: list[dict]) -> list[dict]:
    data_format_list = []
    for item in list_to_format:
        data_format_list.append(
            {
                "event": item["event"],
                "date": datetime.strptime(item["date"], "%Y-%m-%d").date(),
                "time": datetime.strptime(item["time"], "%H:%M").time(),
            }
        )
    return data_format_list


def format_datetype_to_str(list_to_format: list[dict]) -> list[dict]:
    list_for_add = []
    for item in list_to_format:
        list_for_add.append(
            {
                "event": item["event"],
                "date": item["date"].strftime("%Y-%m-%d"),
                "time": item["time"].strftime("%H:%M"),
            }
        )
    return list_for_add


def get_data_from_file() -> list[dict]:
    try:
        with open(data_file, "r") as file:
            data = json.load(file)
            return format_to_datetype(data)
    except FileNotFoundError as err:
        logging.error(err)
        return []
    except JSONDecodeError as err:
        logging.error(err)
        return []


def add_data_to_file(event: str, date: str, time: str) -> None:
    data_from_file = get_data_from_file()
    for item in data_from_file:
        if (
            item["event"] == event
            and item["date"] == datetime.strptime(date, "%Y-%m-%d").date()
            and item["time"] == datetime.strptime(time, "%H:%M").time()
        ):
            raise Exception(
                "Запись с такими данными уже существует. Сначала надо удалить существующую запись."
            )
    data_from_file.append(
        {
            "event": event,
            "date": datetime.strptime(date, "%Y-%m-%d").date(),
            "time": datetime.strptime(time, "%H:%M").time(),
        }
    )
    with open(data_file, "w") as file:
        file.write(
            json.dumps(
                sorted(format_datetype_to_str(data_from_file), key=preparing_to_sort),
                indent=0,
            )
        )


def delete_data_from_file(event: str, date: str, time: str) -> None:
    data_from_file = get_data_from_file()
    for item in data_from_file:
        if (
            item["event"] == event
            and item["date"] == datetime.strptime(date, "%Y-%m-%d").date()
            and item["time"] == datetime.strptime(time, "%H:%M:%S").time()
        ):
            data_from_file.remove(item)
            break
    with open(data_file, "w") as file:
        file.write(
            json.dumps(
                sorted(format_datetype_to_str(data_from_file), key=preparing_to_sort),
                indent=0,
            )
        )
