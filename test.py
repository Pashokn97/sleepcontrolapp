from datetime import datetime

from flask_testing import TestCase

from settings import data_file
from sleepcontrolapp import sleep_app, add_data_to_file
from services import get_data_from_file


class ServiceTest(TestCase):
    def setUp(self) -> None:
        with open(data_file, "w") as file:
            pass

    def create_app(self):
        app = sleep_app
        app.config["TESTING"] = True
        return app

    def test_get(self):
        self.assertEqual(len(get_data_from_file()), 0)

    def test_add(self):
        self.assertEqual(len(get_data_from_file()), 0)
        add_data_to_file("up", "2023-07-30", "10:20")
        self.assertEqual(len(get_data_from_file()), 1)

    def test_order(self):
        add_data_to_file("up", "2023-10-30", "08:20")
        add_data_to_file("up", "2023-10-30", "10:20")
        add_data_to_file("up", "2023-09-30", "10:30")
        self.assertEqual(len(get_data_from_file()), 3)
        self.assertEqual(
            get_data_from_file()[0],
            {
                "event": "up",
                "date": datetime.strptime("2023-09-30", "%Y-%m-%d").date(),
                "time": datetime.strptime("10:30", "%H:%M").time(),
            },
        )
        self.assertEqual(
            get_data_from_file()[1],
            {
                "event": "up",
                "date": datetime.strptime("2023-10-30", "%Y-%m-%d").date(),
                "time": datetime.strptime("08:20", "%H:%M").time(),
            },
        )
        self.assertEqual(
            get_data_from_file()[2],
            {
                "event": "up",
                "date": datetime.strptime("2023-10-30", "%Y-%m-%d").date(),
                "time": datetime.strptime("10:20", "%H:%M").time(),
            },
        )

    def test_broken_file(self):
        self.assertEqual(get_data_from_file(), [])


class ViewsTest(TestCase):
    def create_app(self):
        app = sleep_app
        app.config["TESTING"] = True
        return app

    def test_get_view(self):
        response = self.client.get("/")
        self.assertEqual(response.status_code, 200)

    def test_add_view(self):
        response_get = self.client.get("/add")
        self.assertEqual(response_get.status_code, 200)
        response_post = self.client.post("/add")
        self.assertEqual(response_post.status_code, 400)
